#!/bin/bash
#https://www.manageiq.org/docs/reference/latest/api/examples/create_provider.html
docker run -ti --rm --network="miqnet" -v "//mnt/$PWD:/tmp" alpine sh -xc 'apk add curl;cd /tmp; curl -d@add_provider.json -X POST -ku admin:smartvm https://miq/api/providers'
