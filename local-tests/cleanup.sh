#!/bin/bash
docker stop redfish
docker stop miq
docker rm redfish
docker rm miq
docker network rm miqnet
rm Gemfile.lock
