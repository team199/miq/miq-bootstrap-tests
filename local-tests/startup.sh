#!/bin/bash
docker network create miqnet || true
docker run -d --network="miqnet" --rm -p 8000:8000 -v "//mnt/$PWD:/app" -ti --name redfish -e BIND=0.0.0.0 -e PORT=8000 -e USER=admin -e PASS=passadmin -e SSL=FALSE -e RECORDING=lenovo-sr650 ruby:2.5-alpine sh -xc '[ $SSL = "TRUE" ] && SSLCMD="--ssl" || SSLCMD="";cd /app;rm Gemfile.lock; bundle install; bundle exec redfish serve $SSLCMD --user $USER --pass $PASS --bind $BIND --port $PORT $RECORDING'
docker run --network="miqnet" -p 4000:4000 -p 3000:3000 -p 8443:443 --rm --name miq manageiq/manageiq:oparin-1.1

